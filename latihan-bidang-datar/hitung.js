function hitungSegitiga() {
    let alasDom = document.getElementById("alas");
    let tinggiDom = document.getElementById("tinggi");
    let sisiDom = document.getElementById("sisi");

    let luas = (parseFloat(alasDom.value) * parseFloat(tinggiDom.value)) / 2;
    let keliling =
        parseFloat(alasDom.value) +
        parseFloat(tinggiDom.value) +
        parseFloat(sisiDom.value);
    document.getElementById("hasilLuas").value = luas;
    document.getElementById("hasilKeliling").value = keliling;
}

function hitungPersegi() {
    let sisiDom = document.getElementById("sisi");

    let luas = parseFloat(sisiDom.value) * parseFloat(sisiDom.value);
    let keliling = 4 * parseFloat(sisiDom.value);
    document.getElementById("hasilLuas").value = luas;
    document.getElementById("hasilKeliling").value = keliling;
}

function hitungLingkaran() {
    let jariDom = document.getElementById("jari");

    let luas = 3.14 * parseFloat(jariDom.value) * parseFloat(jariDom.value);
    let diagonal = 2 * parseFloat(jariDom.value);
    let keliling = 3.14 * diagonal;
    document.getElementById("hasilLuas").value = Luas;
    document.getElementById("hasilKeliling").value = Keliling;
}

function hitungTrapesium() {
    let aDom = document.getElementById("a")
    let bDom = document.getElementById("b")
    let tinggiDom = document.getElementById("tinggi")
    let abDom = document.getElementById("ab")
    let bcDom = document.getElementById("bc")
    let cdDom = document.getElementById("cd")
    let daDom = document.getElementById("da")

    let luas = (parseFloat(aDom.value) + parseFloat(bDom.value)) * parseFloat(tinggiDom.value) / 2
    let keliling = parseFloat(abDom.value) + parseFloat(bcDom.value) + parseFloat(cdDom.value) + parseFloat(daDom.value)
    document.getElementById("hasilLuas").value = luas
    document.getElementById("hasilKeliling").value = keliling
}

function hitungJargen() {
    let alasDom = document.getElementById("alas");
    let tinggiDom = document.getElementById("tinggi");

    let luas = parseFloat(alasDom.value) * parseFloat(tinggiDom.value);
    let keliling = (parseFloat(alasDom.value) + parseFloat(tinggiDom.value)) * 2;
    document.getElementById("hasilLuas").value = luas;
    document.getElementById("hasilKeliling").value = keliling;
}

function show() {
    var pilihan = document.getElementById("pilihBangun").bidang.value;
    if (pilihan == "segitiga") {
        let segitiga = `<h2>Segitiga</h2>
  
        <div class="container">
          <div class="row">
            <div class="col-25">
              <label>Alas: </label>
                <input type="number" id="alas" name="alas" placeholder="Alas"><br>
                <label>Tinggi: </label>
                <input type="number" id="tinggi" name="tinggi" placeholder="Tinggi"><br>
                <label>Sisi: </label>
                <input type="number" id="sisi" name="sisi" placeholder="Sisi"><br>
                <div class="row">
                    <input type="submit" value="Kalkulasi" id="btnHitung" value="Hitung Luas" onclick="hitungSegitiga();"><br>
                    Luas : <input type="number" id="hasilLuas" name="hasilLuas"> <br>
                    Keliling : <input type="number" id="hasilKeliling" name="hasilKeliling">
    
                  </div>
                </div>
            </div>
          </div>`;

        document.getElementById("isiPilihan").innerHTML = segitiga;

    } else if (pilihan == "persegi") {
        let persegi = `<h2>Persegi</h2>
  
            <div class="container">
              <div class="row">
                <div class="col-25">
                  <label for="sisi">Sisi: </label>
                    <input type="number" id="sisi" name="sisi" placeholder="Sisi">
                    <div class="row">
                        <input type="submit" value="Kalkulasi" id="btnHitung" value="Hitung Luas" onclick="hitungPersegi();"><br>
                        Luas : <input type="number" id="hasilLuas" name="hasilLuas"> <br>
                        Keliling : <input type="number" id="hasilKeliling" name="hasilKeliling">
                      </div>
                    </div>
                </div>
              </div>`;

        document.getElementById("isiPilihan").innerHTML = persegi;

    } else if (pilihan == "lingkaran") {
        let lingkaran = `<h2>Lingkaran</h2>
  
        <div class="container">
          <div class="row">
            <div class="col-25">
              <label>Jari-jari: </label>
                <input type="number" id="jari" name="jari" placeholder="Jari-jari">
                <div class="row">
                    <input type="submit" value="Kalkulasi" id="btnHitung" value="Hitung Luas" onclick="hitungLingkaran();"><br>
                    Luas : <input type="number" id="hasilLuas" name="hasilLuas"> <br>
                    Keliling : <input type="number" id="hasilKeliling" name="hasilKeliling">
                  </div>
                </div>
            </div>
          </div>`;

        document.getElementById("isiPilihan").innerHTML = lingkaran;

    } else if (pilihan == "trapesium") {
        let trapesium = `    <h2>Trapesium</h2>
  
        <div class="container">
          <div class="row">
            <div class="col-25">
              <label>a: </label>
                <input type="number" id="a" name="a" placeholder="A"><br>
                <label>b: </label>
                <input type="number" id="b" name="b" placeholder="b"><br>
                <label>Tinggi: </label>
                <input type="number" id="tinggi" name="tinggi" placeholder="Tinggi"><br>
                <label>ab: </label>
                <input type="number" id="ab" name="ab" placeholder="ab"><br>
                <label>bc: </label>
                <input type="number" id="bc" name="bc" placeholder="bc"><br>
                <label>cd: </label>
                <input type="number" id="cd" name="cd" placeholder="cd"><br>
                <label>da: </label>
                <input type="number" id="da" name="da" placeholder="da"><br>
                <div class="row">
                    <input type="submit" value="Kalkulasi" id="btnHitung" value="Hitung Luas" onclick="hitungTrapesium();"><br>
                    Luas : <input type="number" id="hasilLuas" name="hasilLuas"> <br>
                    Keliling : <input type="number" id="hasilKeliling" name="hasilKeliling">
    
                  </div>
                </div>
            </div>
          </div>`;
        document.getElementById("isiPilihan").innerHTML = trapesium;

    } else if (pilihan == "jajargenjang") {
        let jargen = `<h2>Jajar Genjang</h2>

            <div class="container">
            <div class="row">
                <div class="col-25">
                <label>Alas:   </label>
                <input type="number" id="alas" name="alas" placeholder="Alas">
                <label>Tinggi: </label>
                <input type="number" id="tinggi" name="tinggi" placeholder="Tinggi">
                <div class="row">
                    <input type="submit" value="Kalkulasi" id="btnHitung" value="Hitung Luas" onclick="hitungJargen();"><br>
                    Luas : <input type="text" id="hasilLuas" name="hasilLuas"> <br>
                    Keliling : <input type="text" id="hasilKeliling" name="hasilKeliling">
                </div>
                </div>
            </div>
            </div>`
        document.getElementById("isiPilihan").innerHTML = jargen;
    } else if (pilihan == "kosong") {
        let kosong = 
        `<h1> Silahkan Pilih Bangun Datar </h1>`
        document.getElementById("isiPilihan").innerHTML = kosong;
    }
}
