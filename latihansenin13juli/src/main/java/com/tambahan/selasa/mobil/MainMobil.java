package com.tambahan.selasa.mobil;

public class MainMobil {
    public static void main(String[] args) {
        ClassMobil mobil = new ClassMobil();
        mobil.setTahun(1995);
        mobil.setMerek("Honda");
        mobil.tambahKecepatan(50);
        mobil.kurangKecepatan(50);

        System.out.println(mobil.getTahun());
        System.out.println(mobil.getMerek());

    }
}
