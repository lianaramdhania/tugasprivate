package com.tambahan.selasa;

public class NilaiAkhirMahasiswa {
    public static void main(String[] args) {
        String[] nama = {"Ardi", "Pian", "Robi"};
        int [][] nilai1 = { {60, 70, 90}, {80, 70, 90}, {70, 60, 90} };
        double nilai = 0;

        System.out.println("+-------+-------+-------+-------+-------------+");
        System.out.println("|  Nama |  UTS  |  UAS  | TUGAS | NILAI AKHIR |");
        System.out.println("+-------+-------+-------+-------+-------------+");

        for (int row = 0; row < 3; row++){
            System.out.print("| " +  nama[row] + " \t|  ");
            for (int column = 0; column < 3; column++){
                System.out.print(nilai1[row][column] + "\t| ");
            }
          nilai = (0.35 * nilai1[row][0]) + (0.45 * nilai1[row][1]) + (0.2 * nilai1[row][2]);
          System.out.println(nilai + "\t      |");
        }
        System.out.println("+-------+-------+-------+-------+-------------+");


    }
}
