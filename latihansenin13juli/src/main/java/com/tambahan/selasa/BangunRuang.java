package com.tambahan.selasa;

import java.util.Scanner;

public class BangunRuang {
    public static void main(String[] args) {
        //MENGHITUNG BALOK
        System.out.println("Balok");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Panjang: ");
        double panjang = scanner.nextDouble();

        System.out.print("Masukkan Tinggi: ");
        double tinggi = scanner.nextDouble();

        System.out.print("Masukkan Lebar: ");
        double lebar = scanner.nextDouble();

        double volumebalok = panjang * lebar * tinggi;

        System.out.println("Hasil:" + volumebalok);
        System.out.println(" ");

        //MENGHITUNG BOLA
        System.out.println("Bola");
        System.out.print("Masukkan Diameter: ");
        double diameter = scanner.nextDouble();

        double jari = diameter/2;
        double phi = 3.14;
        double volumebola = jari * jari * jari * phi * 4/3;

        System.out.println("Hasil Volume: " + volumebola);
        System.out.println(" ");

        //KUBUS
        System.out.println("Kubus");
        System.out.print("Masukkan Rusuk: ");
        double rusuk = scanner.nextDouble();

        double volumekubus = rusuk * rusuk * rusuk;

        System.out.println("Volume Kubus: " + volumekubus);
        System.out.println(" ");

        //Summary
        double summary = volumebalok + volumebola + volumekubus;
        System.out.println("Summary: " + summary);

        //Average
        double average = (volumebalok + volumebola + volumekubus) / 3;
        System.out.println("Average: " + average);



    }

}
