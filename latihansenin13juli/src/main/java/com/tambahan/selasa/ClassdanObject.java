package com.tambahan.selasa;


/*
class adalah template untuk menjelaskan status dan perilaku (states dan behaviour)
object adalah objek itu memiliki status dan perilaku (state dan behaviour)
states : kucing memiliki warna, nama, telinga, bulu
behaviour : kucing itu makan, mengengong, bermain

states itu value awal
 */

public class ClassdanObject {
    public ClassdanObject(String jenis) {
        System.out.println("Jenis Kucing Dia: " + jenis);
    }
    public static void main(String[] args) {
    ClassdanObject cdo = new ClassdanObject("Persia");
    }
    }


