package com.tambahan.senin.soal1;

import java.util.Scanner;

public class MembuatKalkulator {
    public static void main(String[] args) {
        System.out.println("==========Soal 1==========");

    Scanner scanner = new Scanner(System.in);
    System.out.print("Masukkan Bilangan 1 : ");
    float a = scanner.nextFloat();
    System.out.print("Masukkan Bilangan 2 : ");
    float b = scanner.nextFloat();

    System.out.println("Masukkan pilihan : 1. Perkalian 2. Pembagian 3. Pertambahan 4. Perkurangan 5. Modulus");
    Integer pilihan = scanner.nextInt();

    if (pilihan == 1 ){
        float perkalian = a * b;
        System.out.println("Hasil : " + perkalian);

    } else if (pilihan == 2) {
        float pembagian = a / b;
        System.out.println("Hasil : " + pembagian);

    } else if (pilihan == 3) {
        float pertambahan = a + b;
        System.out.println("Hasil" + pertambahan);

    } else if (pilihan == 4) {
        float perkurangan = a - b;
        System.out.println("Hasil : " + perkurangan);

    } else if (pilihan == 5) {
        float modulus = a % b;
        System.out.println("Hasil : " + modulus);
    }

    }
}
