package com.tambahan.senin.soal2;

import java.util.Scanner;

public class MembuatPiramida {
    public static void main(String[] args) {
        System.out.println("==========Soal 2==========");
        System.out.println("Membuat Piramida");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Bilangan : ");
        Integer bilangan = scanner.nextInt();

        //pengulangan ke bawah membentuk baris
        for(int a = 0; a < bilangan; a++) {
            System.out.println();

            //ini untuk membentuk lubang di samping
            for (int b = bilangan; b > a - 1; b--) {
                System.out.print(" ");
            }

            //ini untuk membentuk bintang miring kiri
            for (int c = 0; c <= a - 1; c++) {
                System.out.print("*");

            }

            for (int d = 0; d <= a; d++) {
                System.out.print("-");
            }
        }


    }
}
