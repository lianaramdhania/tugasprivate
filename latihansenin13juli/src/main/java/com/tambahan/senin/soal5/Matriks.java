package com.tambahan.senin.soal5;

public class Matriks {
    public static void main(String[] args) {
        System.out.println("==========Soal 5==========");

        int matriksA[][] = {
                {7, 9, 5},
                {1, 5, 0},
                {4, 1, 2}};
        int matriksB[][] = {
                {5, 7, 3},
                {0, 4, 6},
                {3, 4, 5}};

        System.out.println("Matriks A");
        for (int i = 0; i < matriksA.length; i++) {
            for (int j = 0; j < matriksA.length; j++) {
                System.out.print(matriksA[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("Matriks B");
        for (int k = 0; k < matriksB.length; k++) {
            for (int l = 0; l < matriksB.length; l++) {
                System.out.print(matriksB[k][l] + " ");
            }
            System.out.println();
        }
        System.out.println();

        int matriksC[][] = new int[matriksA.length][matriksB.length];
        System.out.println("Hasil Penjumlahan");
        for (int m = 0; m < matriksC.length; m++) {
            for (int n = 0; n < matriksC.length; n++) {
                matriksC[m][n] = matriksA[m][n] + matriksB[m][n];
                System.out.print(matriksC[m][n] + "  ");
            }
            System.out.println();
        }

        System.out.println();
        System.out.println("Hasil Pengurangan");
        System.out.println();
        for (int m = 0; m < matriksC.length; m++) {
            for (int n = 0; n < matriksC.length; n++) {
                matriksC[m][n] = matriksA[m][n] - matriksB[m][n];
                System.out.print(matriksC[m][n] + "  ");
            }
            System.out.println();
        }

    }
}
