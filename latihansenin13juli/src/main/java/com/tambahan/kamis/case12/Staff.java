package com.tambahan.kamis.case12;

public class Staff extends Worker{
    int tunMakan = 20000;

    public Staff(int idK, String nama, int gajipokok) {
        super(idK, nama, gajipokok);
    }

    public int getTunM() {
        return tunMakan * super.getAbsen();
    }

    public void setTunM(int tunM) {
        this.tunMakan = tunM;
    }
}
