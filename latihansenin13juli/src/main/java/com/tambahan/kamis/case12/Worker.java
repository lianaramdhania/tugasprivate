package com.tambahan.kamis.case12;

public class Worker {
    int idK;
    String nama;
    int absen;
    int gajipokok;
    String jabatan;

    public Worker(int idK, String nama, int gajipokok) {
        this.idK = idK;
        this.nama = nama;
        this.gajipokok = gajipokok;
    }

    public int getIdK() {
        return idK;
    }

    public void setIdK(int idK) {
        this.idK = idK;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }

    public int getGajipokok() {
        return gajipokok;
    }

    public void setGajipokok(int gajipokok) {
        this.gajipokok = gajipokok;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
}
