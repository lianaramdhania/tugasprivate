package com.tambahan.kamis.cobaarray;

import com.tambahan.kamis.salah.Worker1;

public class ListKaryawan extends Worker1 {
    ListKaryawan(int id, String nama, String jabatan, float gajipokok){
        super.setIdKaryawan(id);
        super.setNama(nama);
        super.setJabatan(jabatan);
        super.setGajipokok(gajipokok);

    }

    public void showKaryawan(){
        System.out.println("ID: " + super.getIdKaryawan());
        System.out.println("Nama: " + super.getNama());
        System.out.println("Jabatan: " + super.getJabatan());
        System.out.println("Gaji Pokok: " + super.getGajipokok());
    }
}
