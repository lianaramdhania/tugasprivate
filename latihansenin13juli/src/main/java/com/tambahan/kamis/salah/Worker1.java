package com.tambahan.kamis.salah;

public class Worker1 {
        int idKaryawan;
        String nama;
        double pulsa;
        float gajipokok;
        int absen;
        String jabatan;

    public int getIdKaryawan() {
        return idKaryawan;
    }

    public void setIdKaryawan(int idKaryawan) {
        this.idKaryawan = idKaryawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public double getPulsa() {
        return pulsa;
    }

    public void setPulsa(double pulsa) {
        this.pulsa = pulsa;
    }

    public int getAbsen() {
        return absen;
    }

    public void setAbsen(int absen) {
        this.absen = absen;
    }

    public double getGajipokok() {
        return gajipokok;
    }

    public void setGajipokok(float gajipokok) {
        this.gajipokok = gajipokok;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }


}

