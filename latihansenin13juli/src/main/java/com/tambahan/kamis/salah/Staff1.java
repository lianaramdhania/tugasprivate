package com.tambahan.kamis.salah;

public class Staff1 extends Worker1 {
    private double tunjanganMakan = 20000;

    public void setTunjanganMakan(double tunjanganMakan) {
        this.tunjanganMakan = tunjanganMakan;
    }

    public double getTunjanganMakan() {
        return this.tunjanganMakan * super.getAbsen();

    }
}
