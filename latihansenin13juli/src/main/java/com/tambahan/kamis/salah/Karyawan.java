package com.tambahan.kamis.salah;

class Karyawan {
    int idKaryawan;
    String nama;
    float gajipokok;
    String jabatan;

    public Karyawan(int idKaryawan, String nama, String jabatan, float gajipokok)
    { this.idKaryawan = idKaryawan;
    this.nama = nama;
    this.jabatan = jabatan;
    this.gajipokok = gajipokok;
    }

    public int getIdKaryawan()
    {
        return idKaryawan;
    }

    public String getNama()
    {
        return nama;
    }

    public String getJabatan()
    {
        return jabatan;
    }

    public float getGajipokok()
    {
        return gajipokok;
    }
}
