package com.tambahan.kamis.salah;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Program Data Karyawan" + "\n" + "1. Tampilkan Data" + "\n" + "2. Total Tunjangan" + "\n" + "3. Exit");
            System.out.print("Pilih: ");
            Integer menu = scanner.nextInt();


            switch (menu) {
                case 1:
                    System.out.println("");
                    System.out.println("Data Karyawan");
                    TampilData tampilData = new TampilData();
                    tampilData.hasilData();
                    System.out.println("");
                    break;

                case 2:
                    TampilData tampilData1 = new TampilData();
                    ArrayList<Karyawan> show = tampilData1.getDaftarKaryawan();
                    System.out.println("");
                    Staff1 worker = new Staff1();
                    System.out.print("Masukkan Absen: ");
                    worker.setAbsen(scanner.nextInt());
                    System.out.println(worker.getTunjanganMakan());
                    System.out.println("");
                    break;

                case 3:
                    System.exit(0);
            }
        }
    }
}