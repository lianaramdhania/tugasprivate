package com.tambahan.kamis.salah;

import java.util.ArrayList;

public class TampilData {
    ArrayList<Karyawan> show;

    public TampilData()
    {
        show =new ArrayList<Karyawan>();
    }

    public void isiData(int idKaryawan, String nama, String jabatan, float gajipokok){
        show.add(new Karyawan(idKaryawan, nama, jabatan, gajipokok));
    }

    public void showData(){
        for (Karyawan karyawan : show){
            System.out.println("ID Karyawan: " + karyawan.getIdKaryawan() + " " + "Nama: " + karyawan.getNama() + " " + "Jabatan" + karyawan.getJabatan() + " " + "Gaji Pokok: " + karyawan.getGajipokok());
        }
    }

    public void hasilData(){
        TampilData tampilData = new TampilData();
        tampilData.isiData(1, "Alya", "Manager", 1000000);
        tampilData.isiData(2, "Bara", "Manager", 1000000);
        tampilData.isiData(3, "Chelsea", "Staff", 5000000);
        tampilData.isiData(4, "Dean", "Staff", 5000000);
        tampilData.isiData(5, "Edza", "Staff", 5000000);
        tampilData.showData();
    }
    public ArrayList<Karyawan> getDaftarKaryawan(){
        return show;
    }
}
