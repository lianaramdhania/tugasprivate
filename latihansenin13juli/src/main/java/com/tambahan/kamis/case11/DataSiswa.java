package com.tambahan.kamis.case11;

import java.util.ArrayList;
import java.util.Scanner;

public class DataSiswa {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> idSiswa = new ArrayList<Integer>();
        ArrayList<String> namaSiswa = new ArrayList<String>();
        ArrayList<Double> nilaiSiswa = new ArrayList<Double>();
        ArrayList<String> penilaian = new ArrayList<String>();

        while (true) {
        System.out.println("Menu Data Siswa" + "\n" + "1. Tambah Siswa" + "\n" + "2. Edit Data Siswa" + "\n" + "3. Menghapus Data Siswa" + "\n" + "4. Laporan Data Siswa" + "\n" + "5. Exit");
        System.out.print("Pilih: ");
        Integer menu = scanner.nextInt();


            switch (menu) {
                case 1:
                        System.out.println("");
                        System.out.println("Masukkan Data Siswa");
                        System.out.print("ID: ");
                        Integer id = scanner.nextInt();
                        System.out.print("Nama: ");
                        String nama = scanner.next();
                        System.out.print("Nilai: ");
                        double nilai = scanner.nextDouble();
                        idSiswa.add(id);
                        namaSiswa.add(nama);
                        nilaiSiswa.add(nilai);

                        String huruf;
                        if (nilai <= 20){
                            huruf = "E";
                            penilaian.add(huruf);
                        } else if (nilai > 20 && nilai <= 40 ){
                            huruf = "D";
                            penilaian.add(huruf);
                        } else if (nilai > 40 && nilai <= 60) {
                            huruf = "C";
                            penilaian.add(huruf);
                        } else if (nilai > 60 && nilai <= 80 ) {
                            huruf = "B";
                            penilaian.add(huruf);
                        } else if (nilai > 80 && nilai <= 100) {
                            huruf = "A";
                            penilaian.add(huruf);
                        }
                    System.out.println("Data Berhasil Ditambahkan");
                    System.out.println("");
                    break;

                case 2:
                    System.out.println("");
                    System.out.print("Cari Berdasarkan ID: ");
                    int cari = scanner.nextInt();
                    System.out.println("");
                    System.out.println("Data Siswa" + "\n" + "ID Siswa: " + idSiswa.get(cari - 1) + "\n" + "Nama Siswa: " + namaSiswa.get(cari - 1) + "\n" + "Nilai Siswa Sebelum Diubah: " + nilaiSiswa.get(cari - 1) + "\n" + "Grade: " + penilaian.get(cari - 1));

                    System.out.println("\n" + "Masukkan Nilai Baru: ");
                    double nilaiB = scanner.nextDouble();
                    nilaiSiswa.set(cari-1, nilaiB);
                    System.out.println("\n" + "Nilai Setelah Diubah: " + nilaiSiswa.get(cari - 1));

                    String huruf2;
                    if (nilaiB <= 20){
                        huruf2 = "E";
                        penilaian.set(cari - 1, huruf2);
                    } else if (nilaiB > 20 && nilaiB <= 40 ){
                        huruf2 = "D";
                        penilaian.set(cari - 1, huruf2);
                    } else if (nilaiB > 40 && nilaiB <= 60) {
                        huruf2 = "C";
                        penilaian.set(cari - 1, huruf2);
                    } else if (nilaiB > 60 && nilaiB <= 80 ) {
                        huruf2 = "B";
                        penilaian.set(cari - 1, huruf2);
                    } else if (nilaiB > 80 && nilaiB <= 100) {
                        huruf2 = "A";
                        penilaian.set(cari - 1, huruf2);
                    }
                    System.out.println("");
                    break;

                case 3:
                    System.out.println("");
                    System.out.println("Masukkan ID untuk Menghapus Salah Satunya: ");
                    Integer hapus = scanner.nextInt();
                    idSiswa.remove(hapus-1);
                    namaSiswa.remove(hapus-1);
                    nilaiSiswa.remove(hapus-1);
                    break;

                case 4:
                    System.out.println("");
                    System.out.println("+-------+------------+----------+-----------+");
                    System.out.println("|   ID  | NAMA SISWA |  NILAI   |   GRADE   |");
                    System.out.println("+-------+------------+----------+-----------+");
                    for (int i = 0; i < idSiswa.size(); i++) {
                        System.out.print("| \t" + idSiswa.get(i) + "\t");

                        System.out.print("|\t  " + namaSiswa.get(i) + "\t |");

                        System.out.print("\t" + nilaiSiswa.get(i) + "\t|");

                        System.out.print("\t  "+ penilaian.get(i) + "\t\t\n|");
                    }
                    System.out.println("+-------+------------+----------+-----------+");
                    System.out.println("");
                    break;
                case 5:
                    System.exit(0);
                default:
                    System.out.println("Salah");
                    }


            }
        }
    }

