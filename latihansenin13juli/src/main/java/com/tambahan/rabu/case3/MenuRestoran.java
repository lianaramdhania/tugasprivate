package com.tambahan.rabu.case3;

import java.util.Scanner;

public class MenuRestoran {
    public static void main(String[] args) {

        System.out.println("           Menu Restaurant Mc'Cihuy         ");
        System.out.println("===========================================");
        System.out.println("1. Nasi Goreng Informatika     Rp. 5.000,-");
        System.out.println("2. Nasi Soto Ayam Internet     Rp. 7.000,-");
        System.out.println("3. Gado-Gado Disket            Rp. 4.000,-");
        System.out.println("4. Bubur Ayam LAN              Rp. 4.000,-");
        System.out.println("===========================================");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Pilihan Anda: ");
        Integer pilihan = scanner.nextInt();

        switch (pilihan) {
            case 1:
                System.out.println("1. Nasi Goreng Informatika     Rp. 5.000,-");
                break;
            case 2:
                System.out.println("2. Nasi Soto Ayam Internet     Rp. 7.000,-");
                break;
            case 3:
                System.out.println("3. Gado-Gado Disket            Rp. 4.000,-");
                break;
            case 4:
                System.out.println("4. Bubur Ayam LAN              Rp. 4.000,-");
                break;
            default:
                System.out.println("Mohon Maaf, Pilihan yang Anda Pilih Tidak Tersedia");

        }
    }
}

