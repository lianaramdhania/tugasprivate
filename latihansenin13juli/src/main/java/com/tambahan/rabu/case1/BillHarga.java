package com.tambahan.rabu.case1;

import java.util.Scanner;

public class BillHarga {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Harga Pembelian: ");
        double harga = scanner.nextDouble();

        double diskon = harga / 100 * 10;
        double hargabayar = harga - diskon;

        System.out.println("Diskon: " + diskon);
        System.out.println("Harga Bayar: " + hargabayar);
        }
    }
