package com.tambahan.rabu.case6;

import java.util.Scanner;

public class MenentukanMusim {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Bulan: ");
        Integer bulan = scanner.nextInt();

        if (bulan == 12 || bulan <= 2) {
            System.out.println("Musim Dingin");
        } else if (bulan <= 5) {
            System.out.println("Musim Semi");
        } else if (bulan <= 9) {
            System.out.println("Musim Panas");
        } else if (bulan <= 11) {
            System.out.println("Musim Gugur");
        }
    }
}
