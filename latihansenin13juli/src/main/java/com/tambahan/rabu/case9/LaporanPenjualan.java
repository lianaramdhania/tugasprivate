package com.tambahan.rabu.case9;

import java.io.DataInputStream;
import java.util.Scanner;

public class LaporanPenjualan {
        public static void main(String[] args) throws Exception
        {
//            Scanner scanner = new Scanner(System.in);
            String[] nama = new String[10];
            int[]jumlah = new int[10];
            int[]harga = new int[10];
            DataInputStream karakter = new DataInputStream(System.in);
            System.out.print("Masukan Bulan Penjualan : ");
            String bulan=karakter.readLine();
            System.out.print("Masukan jumlah data : ");
            String jml=karakter.readLine();

            int data = Integer.valueOf(jml).intValue();
            for(int i=0;i<data;i++)
            {
                System.out.print("Nama Barang Ke-"+(i+1)+" = ");
                String nm=karakter.readLine();
                nama [i]=nm;
                System.out.print("Jumlah : ");
                String jmlh=karakter.readLine();
                int jlah = Integer.valueOf(jmlh).intValue();
                jumlah[i]=jlah;
                System.out.print("Harga Rp.: ");
                String har = karakter.readLine();
                int harg = Integer.valueOf(har).intValue();
                harga[i]=harg;

            }
            System.out.println("LAPORAN PENJUALAN PT. NEXSOFT");
            System.out.println("BULAN : "+bulan);
            System.out.println("================================================");
            System.out.println("NO  NAMA BARANG     JUMLAH    HARGA    TOTAL    ");
            System.out.println("================================================");

            int tpenj=0;
            for(int i=0;i<data;i++)

            {

                System.out.println((i+1)+"   "+nama[i]+"           "+jumlah[i]+"       "+harga[i]+ "        "+(jumlah[i]*harga[i]));
                tpenj = tpenj + (jumlah[i]*harga[i]);

            }

            System.out.println("================================================");
            System.out.println("TOTAL BARANG : "+data);
            System.out.println("TOTAL PENJUALAN : "+tpenj);

        }

    }
