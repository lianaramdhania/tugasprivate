package com.tambahan.rabu.case5;

import java.util.Arrays;
import java.util.Collections;

public class UrutkanData {
    public static void main(String[] args) {
        Integer[] data = {5, 1, 3, 2, 4};

        System.out.println("Data Asli");
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " ");
        }

        System.out.println();
        System.out.println("Pengurutan Ascending");
        for (int i = 0; i < data.length; i++) {
            Arrays.sort(data);
            System.out.print(data[i] + " ");
        }

        System.out.println();
        System.out.println("Pengurutan Descending");
        for (int i = 0; i < data.length; i++) {
            Arrays.sort(data, Collections.<Integer>reverseOrder());
            System.out.print(data[i] + " ");
        }
    }
}

