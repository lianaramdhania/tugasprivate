package com.tambahan.rabu.case4;

import java.util.Scanner;

public class ProgramSusu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Kode Susu (1-3) : ");
        Integer kode = scanner.nextInt();

        System.out.print("Masukkan Ukuran Susu (B / S / K): ");
        String ukuran = scanner.next();
        System.out.print("Masukkan Jumlah Pembelian: ");
        double jumlah = scanner.nextDouble();

        switch (kode){
            case 1:
                System.out.println("Merek: Susu Indomilk");
                break;
            case 2:
                System.out.println("Merek: Susu Milo");
                break;
            case 3:
                System.out.println("Merek: Susu Ultra");
                break;
            default:
                System.out.println("Mohon Maaf, Kode yang Anda Masukkan Salah");
        }

        double b = 10000;
        double s = 4250;
        double k = 2100;
        double indomilk = 1.5;
        double milo = 3;
        double ultra = 4;


        if (kode == 1 && ukuran.equalsIgnoreCase("B" )) {
            System.out.println("Jumlah Pembelian: " + ((b * indomilk) * jumlah));

        } else if (kode == 1 && ukuran.equalsIgnoreCase("S")) {
            System.out.println("Jumlah Pembelian: " + ((s * indomilk) * jumlah));

        } else if (kode == 1 && ukuran.equalsIgnoreCase("K")) {
            System.out.println("Harga Susu: " + ((k * indomilk) * jumlah));

        }else if (kode == 2 && ukuran.equalsIgnoreCase("B")) {
            System.out.println("Jumlah Pembelian: " + ((b * milo) * jumlah));

        }else if (kode == 2 && ukuran.equalsIgnoreCase("S")) {
            System.out.println("Jumlah Pembelian: " + ((s * milo) * jumlah));

        } else if (kode == 2 && ukuran.equalsIgnoreCase("K")) {
            System.out.println("Jumlah Pembelian: " + ((k * milo) * jumlah));

        }else if (kode == 3 && ukuran.equalsIgnoreCase("B")) {
            System.out.println("Jumlah Pembelian: " + ((b * ultra) * jumlah));

        }else if (kode == 3 && ukuran.equalsIgnoreCase("S")) {
            System.out.println("Jumlah Pembelian: " + ((s * ultra) * jumlah));

        }else if (kode == 3 && ukuran.equalsIgnoreCase("K")) {
            System.out.println("Jumlah Pembelian: " + ((k * ultra) * jumlah));

        }

        int stock = 100;
        System.out.println("Stock Akhir: " + (stock - jumlah));

    }

    }

