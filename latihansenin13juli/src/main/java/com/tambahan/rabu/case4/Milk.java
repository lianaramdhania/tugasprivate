package com.tambahan.rabu.case4;

import java.util.Scanner;

public class Milk {
    int totalHarga;
    String[] nama = {"Bebelac", "Morinaga", "Dancow"};
    int[][] harga = {{200000, 150000, 100000}, //bebelac
            {150000, 130000, 70000}, //Morinaga
            {100000, 70000, 45000}}; //Dancow
    int[][] stok = {{12, 10, 20},
            {20, 20, 40},
            {30, 30, 10}};
    String[] ukuran = {"B", "S", "K"};


    public void belanja() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Kode Susu (1-3): ");
        int kode = scanner.nextInt();
        System.out.print("Masukkan ukuran (B / S / K ) : ");
        String ukuran1 = scanner.next();
        System.out.print("Masukkan Jumlah Pembelian: ");
        int jumlahBeli = scanner.nextInt();
        System.out.println();

        String namaProduk = nama[kode - 1];
        System.out.println("Nama Produk \t: " + namaProduk);

        int i = 0;
        for (; i < ukuran.length; i++) {
            if (ukuran[i].equalsIgnoreCase(ukuran1)) {
                break;
            }
        }

        int hargaSatuan = harga[kode - 1][i];
        System.out.println("Harga Satuan \t: " + hargaSatuan);
        int stokBarang = stok[kode - 1][i];

        if (jumlahBeli > stokBarang) {
            System.out.println("Stok tidak cukup");
        } else {
            totalHarga = hargaSatuan * jumlahBeli;
            System.out.println("Total Bayar \t: " + totalHarga);
        }

    }

    public static void main(String[] args) {
        Milk milk = new Milk();
        milk.belanja();
    }
}