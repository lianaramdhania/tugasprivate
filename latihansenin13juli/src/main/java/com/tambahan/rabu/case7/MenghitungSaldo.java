package com.tambahan.rabu.case7;

import java.util.Scanner;

public class MenghitungSaldo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan Saldo Awal: ");
        double saldoa = scanner.nextDouble();

        double biayadmin = 8000;
        double bunga = saldoa * 0.02;
        double saldos = saldoa - biayadmin + bunga;

        System.out.println("Saldo Awal: " + saldoa);
        System.out.println("Setelah berjalan satu bulan dengan pertimbangan bunga dan biaya admin, saldo baru anda adalah " + saldos);
        }

    }

