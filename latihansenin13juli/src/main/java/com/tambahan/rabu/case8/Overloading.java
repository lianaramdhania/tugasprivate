package com.tambahan.rabu.case8;

public class Overloading {
    public static void main(String[] args) {
        int a = 10;
        int b = 6;
        double c = 7.8;
        double d = 7.3;
        int result1 = minFunction(a, b);
        double result2 = minFunction(c, d);
        System.out.println("Minimum Value = " + result1);
        System.out.println("Minimum Value = " + result2);
    }
    //untuk integer
    public static int minFunction(int n1, int n2) {
        int min;
        if (n1 > n2)
            min = n2;
        else
            min = n1;

        return min;
    }

    //untuk double
    public static double minFunction(double n1, double n2) {
        double min;
        if (n1 > n2)
            min = n2;
        else
            min = n1;

        return min;
    }
}




