package com.tambahan.rabu.case2;

public class AppBillTagihan {
    public static void main(String[] args) {
        BillTagihanListrik bill = new BillTagihanListrik();
        bill.setNomor(001);
        bill.setNama("Kita BIsa");
        bill.setBulan("Juli");
        bill.setTarif(250);
        bill.setBiaya(20000);
        bill.setPulsa(110);
        float tagihan = (bill.getPulsa()*bill.getTarif()) + bill.getBiaya();

        System.out.println("Bill Tagihan Listrik");
        System.out.println("Nomor Pelanggan: " + bill.getNomor());
        System.out.println("Nama Pelanggan: " + bill.getNama());
        System.out.println("Bulan Tagihan: " + bill.getBulan());
        System.out.println("Banyaknya Pulsa Pemakaian: " + bill.getPulsa());
        System.out.println("Jumlah Tagihan Bulan " + bill.getBulan() + " adalah sebesar " + tagihan);

    }
}
