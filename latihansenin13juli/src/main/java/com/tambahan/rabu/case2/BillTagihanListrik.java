package com.tambahan.rabu.case2;

public class BillTagihanListrik {
    private int nomor;
    private String nama;
    private String bulan;
    private float tarif;
    private float biaya;
    private float pulsa;

    public int getNomor() {
        return nomor;
    }

    public void setNomor(int nomor) {
        this.nomor = nomor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }
    public float getTarif() {
        return tarif;
    }

    public void setTarif(float tarif) {
        this.tarif = tarif;
    }

    public float getBiaya() {
        return biaya;
    }

    public void setBiaya(float biaya) {
        this.biaya = biaya;
    }

    public float getPulsa() {
        return pulsa;
    }

    public void setPulsa(float pulsa) {
        this.pulsa = pulsa;
    }

}
